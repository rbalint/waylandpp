Source: waylandpp
Maintainer: Balint Reczey <rbalint@ubuntu.com>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 12),
               cmake,
               pkg-config,
               pkg-kde-tools,
               libpugixml-dev,
               libwayland-dev,
               libwayland-egl1-mesa,
               libegl1-mesa-dev,
               wayland-scanner++:any <cross>,
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/rbalint/waylandpp
Vcs-Git: https://salsa.debian.org/rbalint/waylandpp.git
Homepage: https://github.com/NilsBrause/waylandpp

Package: waylandpp-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libwayland-client++0 (= ${binary:Version}),
         libwayland-client-extra++0 (= ${binary:Version}),
         libwayland-cursor++0 (= ${binary:Version}),
         libwayland-egl++0(= ${binary:Version}),
         wayland-scanner++:any (= ${binary:Version}),
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: wayland compositor infrastructure - C++ development files
 Wayland is a protocol for a compositor to talk to its clients as well
 as a C library implementation of that protocol. The compositor can be
 a standalone display server running on Linux kernel modesetting and
 evdev input devices, an X application, or a wayland client
 itself. The clients can be traditional applications, X servers
 (rootless or fullscreen) or other display servers.
 .
 This package ships the C++ bindings for the development libraries.

Package: wayland-scanner++
Architecture: any
Multi-Arch: allowed
Section: libdevel
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: wayland compositor infrastructure - C++ protocol code generator
 Wayland is a protocol for a compositor to talk to its clients as well
 as a C library implementation of that protocol. The compositor can be
 a standalone display server running on Linux kernel modesetting and
 evdev input devices, an X application, or a wayland client
 itself. The clients can be traditional applications, X servers
 (rootless or fullscreen) or other display servers.
 .
 This package ships the C++ code generator binary.

Package: libwayland-client++0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: wayland compositor infrastructure - client library C++ bindings
 Wayland is a protocol for a compositor to talk to its clients as well
 as a C library implementation of that protocol. The compositor can be
 a standalone display server running on Linux kernel modesetting and
 evdev input devices, an X application, or a wayland client
 itself. The clients can be traditional applications, X servers
 (rootless or fullscreen) or other display servers.
 .
 This package ships the C++ bindings for the client side library of
 the Wayland protocol.

Package: libwayland-client-extra++0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: wayland compositor infrastructure - client library extra C++ bindings
 Wayland is a protocol for a compositor to talk to its clients as well
 as a C library implementation of that protocol. The compositor can be
 a standalone display server running on Linux kernel modesetting and
 evdev input devices, an X application, or a wayland client
 itself. The clients can be traditional applications, X servers
 (rootless or fullscreen) or other display servers.
 .
 This package ships the C++ bindings for the client side libraries of
 the extra Wayland protocols.

Package: libwayland-cursor++0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: wayland compositor infrastructure - cursor library C++ bindings
 Wayland is a protocol for a compositor to talk to its clients as well
 as a C library implementation of that protocol. The compositor can be
 a standalone display server running on Linux kernel modesetting and
 evdev input devices, an X application, or a wayland client
 itself. The clients can be traditional applications, X servers
 (rootless or fullscreen) or other display servers.
 .
 This package ships the C++ bindings for the library of
 the Wayland protocol to manage cursors.

Package: libwayland-egl++0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: wayland compositor infrastructure - EGL library C++ bindings
 Wayland is a protocol for a compositor to talk to its clients as well
 as a C library implementation of that protocol. The compositor can be
 a standalone display server running on Linux kernel modesetting and
 evdev input devices, an X application, or a wayland client
 itself. The clients can be traditional applications, X servers
 (rootless or fullscreen) or other display servers.
 .
 This package ships the C++ bindings for the library which implements the
 Wayland EGL platform of the Wayland protocol.
